import crypto from "node:crypto";

import type {
  AlbumAddTags,
  AlbumGetInfo,
  AlbumGetTags,
  AlbumGetTopTags,
  AlbumInfoRes,
  AlbumRemoveTag,
  AlbumSearch,
  AlbumSearchRes,
  AlbumTagsRes,
  MBIDAlbum,
  NamedAlbum,
} from "~/dataModel/albumFunctions";
import type {
  ArtistAddTags,
  ArtistGetCorrection,
  ArtistGetCorrectionRes,
  ArtistGetInfo,
  ArtistGetInfoRes,
  ArtistGetSimilar,
  ArtistGetSimilarRes,
  ArtistGetTags,
  ArtistGetTagsRes,
  ArtistGetTopAlbums,
  ArtistGetTopAlbumsRes,
  ArtistGetTopTags,
  ArtistGetTopTagsRes,
  ArtistGetTopTracks,
  ArtistGetTopTracksRes,
  ArtistRemoveTag,
  ArtistSearch,
  ArtistSearchRes,
  MBIDArtist,
  NamedArtist,
} from "~/dataModel/artistFunctions";
import type {
  ChartGetTopArtists,
  ChartGetTopTags,
  ChartGetTopTracks,
  ChartTopTagsRes,
  ChartTopTracksRes,
} from "~/dataModel/chartFunctions";
import type {
  GeoArtistsRequest,
  GeoTopArtistsRes,
  GeoTopTracksRes,
  GeoTracksRequest,
} from "~/dataModel/geoFunctions";
import type {
  LibraryArtistsRes,
  LibraryRequest,
} from "~/dataModel/libraryFunctions";
import type {
  ErrorMessage,
  ErrorResponse,
  LastFmConstructor,
  LastFmRequest,
  PostRequest,
  ResponseData,
} from "~/dataModel/sharedFunctions";
import type {
  TagInfoRequest,
  TagInfoRes,
  TagSimilarRequest,
  TagSimilarRes,
  TagTopAlbumsRes,
  TagTopArtistsRes,
  TagTopRequest,
  TagTopTagsRequest,
  TagTopTracksRes,
  TagWeeklyChartRes,
  TopTagsRes,
} from "~/dataModel/tagFunctions";
import type {
  MBIDTrack,
  TrackAddTags,
  TrackGetCorrectionRequest,
  TrackGetCorrectionRes,
  TrackGetInfo,
  TrackGetInfoRes,
  TrackGetSimilar,
  TrackGetSimilarRes,
  TrackGetTags,
  TrackGetTagsRes,
  TrackGetTopTags,
  TrackGetTopTagsRes,
  TrackLove,
  TrackRemoveTag,
  TrackRequest,
  TrackScrobble,
  TrackScrobbleRes,
  TrackSearch,
  TrackSearchRes,
  TrackUnlove,
  TrackUpdateNowPlaying,
  TrackUpdateNowPlayingRes,
} from "~/dataModel/trackFunctions";
import type {
  UserGetFriends,
  UserGetFriendsRes,
  UserGetInfo,
  UserGetInfoRes,
  UserGetLovedTracks,
  UserGetLovedTracksRes,
  UserGetPersonalTags,
  UserGetPersonalTagsRes,
  UserGetRecentTracks,
  UserGetRecentTracksRes,
  UserGetTopAlbums,
  UserGetTopAlbumsRes,
  UserGetTopArtists,
  UserGetTopArtistsRes,
  UserGetTopTags,
  UserGetTopTagsRes,
  UserGetTopTracks,
  UserGetTopTracksRes,
  UserGetWeeklyAlbumChart,
  UserGetWeeklyAlbumChartRes,
  UserGetWeeklyArtistChart,
  UserGetWeeklyArtistChartRes,
  UserGetWeeklyChartList,
  UserGetWeeklyChartListRes,
  UserGetWeeklyTrackChart,
  UserGetWeeklyTrackChartRes,
} from "~/dataModel/userFunctions";

export default class LastFm {
  #apiKey: string | undefined;

  #apiSecret: string | undefined;

  #username: string | undefined;

  #password: string | undefined;

  #sessionKey: string | undefined;

  constructor(opts: LastFmConstructor | undefined) {
    if (opts?.apiKey) {
      this.#apiKey = opts.apiKey;
    }
    if (opts?.apiSecret) {
      this.#apiSecret = opts.apiSecret;
    }
    if (opts?.username) {
      this.#username = opts.username;
    }
    if (opts?.password) {
      this.#password = opts.password;
    }
    if (opts?.sessionKey) {
      this.#sessionKey = opts.sessionKey;
    }
  }

  static async #sendErr(msg: string): Promise<ErrorMessage> {
    // handles error checking within library
    const errMsg: ErrorMessage = { error: 6, message: msg, success: false };
    return new Promise((_, reject) => {
      reject(errMsg);
    });
  }

  static #PostRequestToString(params: LastFmRequest): string {
    return Object.entries(params)
      .sort((a, b) => a[0].localeCompare(b[0]))
      .reduce(
        (sig: string, [key, value]: [string, string | string[]]) =>
          key !== "format"
            ? Array.isArray(value)
              ? value.reduce(
                  (newSig: string, item: string, i) =>
                    `${newSig}[${i.toString()}]${item}`,
                  sig,
                )
              : `${sig}${key}${typeof value !== "undefined" ? value : ""}`
            : sig,
        "",
      );
  }

  static #createSignature(params: LastFmRequest, secret: string): string {
    // create signature by hashing
    const sig: string = LastFm.#PostRequestToString(params) + secret;
    return crypto.createHash("md5").update(sig, "utf8").digest("hex");
  }

  static #urlBuilder(url: URL, params: LastFmRequest): string {
    const parameters = Object.entries(params);
    for (const [key, value] of parameters) {
      url.searchParams.append(key, value?.toString() ?? "");
    }
    return url.toString();
  }

  async #getData<
    Response = Record<string, unknown>,
    Name extends string | number | symbol = string,
  >(
    params: LastFmRequest,
    name: Name,
  ): Promise<ResponseData<Response> | ErrorResponse> {
    const queryUrl = new URL("http://ws.audioscrobbler.com/2.0/");
    const queryParams = {
      ...params,
      api_key: this.#apiKey,
      format: "json",
    };
    try {
      const res = await fetch(LastFm.#urlBuilder(queryUrl, queryParams), {
        method: "GET",
        headers: {
          // biome-ignore lint/nursery/noSecrets: definitely not a secret
          "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
        },
      });
      if (res.ok) {
        const json = (await res.json()) as {
          [P in Name]: ResponseData<Response>;
        };
        json[name].success = true;
        return json[name];
      }
      throw new Error("Error communicating with last.fm");
    } catch (e) {
      if (typeof e === "string") {
        return { success: false, error: new Error(e) };
      }
      if (e instanceof Error) {
        return { success: false, error: e };
      }
      return { success: false, error: new Error("Unknown error") };
    }
  }

  async #postData<Response = Record<string, unknown>>(
    params: LastFmRequest,
  ): Promise<ResponseData<Response> | ErrorResponse> {
    if (!this.#apiKey) {
      throw new Error("No api key available");
    }
    if (!this.#apiSecret) {
      throw new Error("No api secret available");
    }
    const qs: Partial<PostRequest> & LastFmRequest = {
      ...params,
      api_key: this.#apiKey,
      format: "json",
    };
    if (this.#sessionKey) {
      qs.sk = this.#sessionKey;
    }
    qs.api_sig = LastFm.#createSignature(qs, this.#apiSecret);
    try {
      const res = await fetch("https://ws.audioscrobbler.com/2.0/", {
        method: "POST",
        headers: {
          // biome-ignore lint/nursery/noSecrets: definitely not a secret
          "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
        },
        // TS will not see PostParams as Record<string, string> because it has optional fields
        body: new URLSearchParams(qs as unknown as Record<string, string>),
      });
      if (res.ok) {
        const json = (await res.json()) as ResponseData<Response>;
        json.success = true;
        return json;
      }
      throw new Error("Error communicating with last.fm");
    } catch (e) {
      if (typeof e === "string") {
        return { success: false, error: new Error(e) };
      }
      if (e instanceof Error) {
        return { success: false, error: e };
      }
      return { success: false, error: new Error("Unknown error") };
    }
  }

  async #get<
    Response = Record<string, unknown>,
    Name extends string | number | symbol = string,
  >(qs: LastFmRequest, name: Name) {
    return await this.#getData<Response, Name>(qs, name);
  }

  async #post<Response = { success: boolean }>(qs: LastFmRequest) {
    return await this.#postData<Response>(qs);
  }

  public async auth_getMobileSession(): Promise<
    { key: string; success: boolean } | ErrorMessage
  > {
    const qs = {
      method: "auth.getMobileSession",
      username: this.#username,
      password: this.#password,
    };
    try {
      const res = await this.#postData<{
        session: { key: string; success: boolean };
      }>(qs);
      if (!res.success) {
        throw new Error("Error connecting to last.fm");
      }
      this.#sessionKey = res.session.key;
      return res.session;
    } catch (e: unknown) {
      return LastFm.#sendErr(e as string);
    }
  }

  // album methods
  async album_addTags(qs: AlbumAddTags | undefined) {
    if (!(qs?.artist && qs.album && qs.tags)) {
      return LastFm.#sendErr("Missing required params");
    }

    return await this.#post({ ...qs, method: "album.addTags" });
  }

  async album_getInfo(qs: AlbumGetInfo | undefined) {
    if (
      !(
        ((qs as NamedAlbum | undefined)?.artist && (qs as NamedAlbum).album) ||
        (qs as MBIDAlbum | undefined)?.mbid
      )
    ) {
      return LastFm.#sendErr("Missing required params");
    }
    return await this.#get<AlbumInfoRes>(
      { autocorrect: "1", ...qs, method: "album.getInfo" },
      "album",
    );
  }

  async album_getTags(qs: AlbumGetTags | undefined) {
    if (
      !(
        ((qs as NamedAlbum | undefined)?.artist && (qs as NamedAlbum).album) ||
        (qs as MBIDAlbum | undefined)?.mbid
      )
    ) {
      return LastFm.#sendErr("Missing required params");
    }
    return await this.#get<AlbumTagsRes>(
      { autocorrect: "1", ...qs, method: "album.getTags" },
      "tags",
    );
  }

  async album_getTopTags(qs: AlbumGetTopTags | undefined) {
    if (
      !(
        ((qs as NamedAlbum | undefined)?.artist && (qs as NamedAlbum).album) ||
        (qs as MBIDAlbum | undefined)?.mbid
      )
    ) {
      return LastFm.#sendErr("Missing required params");
    }
    return await this.#get<AlbumTagsRes>(
      { autocorrect: "1", ...qs, method: "album.getTopTags" },
      "toptags",
    );
  }

  async album_removeTag(qs: AlbumRemoveTag | undefined) {
    if (!(qs?.artist && qs.album && qs.tag)) {
      return LastFm.#sendErr("Missing required params");
    }
    return await this.#post({ ...qs, method: "album.removeTag" });
  }

  async album_search(qs: AlbumSearch | undefined) {
    if (!qs?.album) {
      return LastFm.#sendErr("Missing album param");
    }
    return await this.#get<AlbumSearchRes>(
      { autocorrect: "1", ...qs, method: "album.search" },
      "results",
    );
  }

  // Artist methods
  async artist_addTags(qs: ArtistAddTags | undefined) {
    if (!(qs?.artist && qs.tags)) {
      return LastFm.#sendErr("Missing required parameters");
    }
    return await this.#post({ ...qs, method: "artist.addTags" });
  }

  async artist_getCorrection(qs: ArtistGetCorrection | undefined) {
    if (!qs?.artist) {
      return LastFm.#sendErr("Missing Artist");
    }
    return await this.#get<ArtistGetCorrectionRes>(
      { ...qs, method: "artist.getCorrection" },
      "corrections",
    );
  }

  async artist_getInfo(qs: ArtistGetInfo | undefined) {
    if (
      !(
        (qs as NamedArtist | undefined)?.artist ||
        (qs as MBIDArtist | undefined)?.mbid
      )
    ) {
      return LastFm.#sendErr("Missing both artist and mbid");
    }

    return await this.#get<ArtistGetInfoRes>(
      { autocorrect: "1", ...qs, method: "artist.getInfo" },
      "artist",
    );
  }

  async artist_getSimilar(qs: ArtistGetSimilar | undefined) {
    if (
      !(
        (qs as NamedArtist | undefined)?.artist ||
        (qs as MBIDArtist | undefined)?.mbid
      )
    ) {
      return LastFm.#sendErr("Missing both artist and mbid");
    }
    return await this.#get<ArtistGetSimilarRes>(
      { autocorrect: "1", ...qs, method: "artist.getSimilar" },
      "similarartists",
    );
  }

  async artist_getTags(qs: ArtistGetTags | undefined) {
    if (
      !(
        qs &&
        ((qs as NamedArtist).artist || (qs as MBIDArtist).mbid) &&
        qs.user
      )
    ) {
      return LastFm.#sendErr("Missing both artist and mbid");
    }
    return await this.#get<ArtistGetTagsRes>(
      { autocorrect: "1", ...qs, method: "artist.getTags" },
      "tags",
    );
  }

  async artist_getTopAlbums(qs: ArtistGetTopAlbums | undefined) {
    if (
      !(
        (qs as NamedArtist | undefined)?.artist ||
        (qs as MBIDArtist | undefined)?.mbid
      )
    ) {
      return LastFm.#sendErr("Missing both artist and mbid");
    }
    return await this.#get<ArtistGetTopAlbumsRes>(
      { autocorrect: "1", ...qs, method: "artist.getTopAlbums" },
      "topalbums",
    );
  }

  async artist_getTopTags(qs: ArtistGetTopTags | undefined) {
    if (
      !(
        (qs as NamedArtist | undefined)?.artist ||
        (qs as MBIDArtist | undefined)?.mbid
      )
    ) {
      return LastFm.#sendErr("Missing both artist and mbid");
    }
    return await this.#get<ArtistGetTopTagsRes>(
      { autocorrect: "1", ...qs, method: "artist.getTopTags" },
      "toptags",
    );
  }

  async artist_getTopTracks(qs: ArtistGetTopTracks | undefined) {
    if (
      !(
        (qs as NamedArtist | undefined)?.artist ||
        (qs as MBIDArtist | undefined)?.mbid
      )
    ) {
      return LastFm.#sendErr("Missing both artist and mbid");
    }
    return await this.#get<ArtistGetTopTracksRes>(
      { autocorrect: "1", ...qs, method: "artist.getTopTracks" },
      "toptracks",
    );
  }

  async artist_removeTag(qs: ArtistRemoveTag | undefined) {
    if (!(qs?.artist && qs.tag)) {
      return LastFm.#sendErr("Missing required parameters");
    }
    return await this.#post({ ...qs, method: "artist.removeTag" });
  }

  async artist_search(qs: ArtistSearch | undefined) {
    if (!qs?.artist) {
      return LastFm.#sendErr("Missing artist to search");
    }
    return await this.#get<ArtistSearchRes>(
      { autocorrect: "1", ...qs, method: "artist.search" },
      "results",
    );
  }

  // Chart Methods
  async chart_getTopArtists(qs: ChartGetTopArtists | undefined) {
    return await this.#get<ChartGetTopArtists>(
      { autocorrect: "1", ...qs, method: "chart.getTopArtists" },
      "artists",
    );
  }

  async chart_getTopTags(qs: ChartGetTopTags | undefined) {
    return await this.#get<ChartTopTagsRes>(
      { autocorrect: "1", ...qs, method: "chart.getTopTags" },
      "tags",
    );
  }

  async chart_getTopTracks(qs: ChartGetTopTracks | undefined) {
    return await this.#get<ChartTopTracksRes>(
      { autocorrect: "1", ...qs, method: "chart.getTopTracks" },
      "tracks",
    );
  }

  // Geo Methods
  async geo_getTopArtists(qs: GeoArtistsRequest | undefined) {
    if (!qs?.country) {
      return LastFm.#sendErr("Missing country");
    }
    return await this.#get<GeoTopArtistsRes>(
      { autocorrect: "1", ...qs, method: "geo.getTopArtists" },
      "topartists",
    );
  }

  async geo_getTopTracks(qs: GeoTracksRequest | undefined) {
    if (!qs?.country) {
      return LastFm.#sendErr("Missing country");
    }
    return await this.#get<GeoTopTracksRes>(
      { autocorrect: "1", ...qs, method: "geo.getTopTracks" },
      "tracks",
    );
  }

  // Library Methods
  async library_getArtists(qs: LibraryRequest | undefined) {
    if (!qs?.user) {
      return LastFm.#sendErr("Missing username");
    }
    return await this.#get<LibraryArtistsRes>(
      { autocorrect: "1", ...qs, method: "library.getArtists" },
      "artists",
    );
  }

  // Tag Methods
  async tag_getInfo(qs: TagInfoRequest | undefined) {
    if (!qs?.tag) {
      return LastFm.#sendErr("No tag given");
    }

    return await this.#get<TagInfoRes>({ ...qs, method: "tag.getInfo" }, "tag");
  }

  async tag_getSimilar(qs: TagSimilarRequest | undefined) {
    if (!qs?.tag) {
      return LastFm.#sendErr("No tag given");
    }

    return await this.#get<TagSimilarRes>(
      { ...qs, method: "tag.getSimilar" },
      "similartags",
    );
  }

  async tag_getTopAlbums(qs: TagTopRequest | undefined) {
    if (!qs?.tag) {
      return LastFm.#sendErr("No tag given");
    }

    return await this.#get<TagTopAlbumsRes>(
      { ...qs, method: "tag.getTopAlbums" },
      "albums",
    );
  }

  async tag_getTopArtists(qs: TagTopRequest | undefined) {
    if (!qs?.tag) {
      return LastFm.#sendErr("No tag given");
    }

    return await this.#get<TagTopArtistsRes>(
      { ...qs, method: "tag.getTopArtists" },
      "topartists",
    );
  }

  async tag_getTopTags(qs: TagTopTagsRequest) {
    return await this.#get<TopTagsRes>(
      { ...qs, method: "tag.getTopTags" },
      "toptags",
    );
  }

  async tag_getTopTracks(qs: TagTopRequest | undefined) {
    if (!qs?.tag) {
      return LastFm.#sendErr("No tag given");
    }

    return await this.#get<TagTopTracksRes>(
      { ...qs, method: "tag.getTopTracks" },
      "tracks",
    );
  }

  async tag_getWeeklyChartList(qs: TagTopRequest | undefined) {
    if (!qs?.tag) {
      return LastFm.#sendErr("No tag given");
    }

    return await this.#get<TagWeeklyChartRes>(
      { ...qs, method: "tag.getWeeklyChartList" },
      "weeklychartlist",
    );
  }

  // Track methods
  async track_addTags(qs: TrackAddTags | undefined) {
    if (!(qs?.artist && qs.track && qs.tags)) {
      return LastFm.#sendErr("Missing required params");
    }

    return await this.#post({ ...qs, method: "track.addTags" });
  }

  async track_getCorrection(qs: TrackGetCorrectionRequest | undefined) {
    if (!(qs?.track && qs.artist)) {
      return LastFm.#sendErr("Missing required params");
    }
    return await this.#get<TrackGetCorrectionRes>(
      { ...qs, method: "track.getCorrection" },
      "corrections",
    );
  }

  async track_getInfo(qs: TrackGetInfo | undefined) {
    if (
      !(
        ((qs as TrackRequest | undefined)?.track &&
          (qs as TrackRequest).artist) ||
        (qs as MBIDTrack | undefined)?.mbid
      )
    ) {
      return LastFm.#sendErr("Missing required params");
    }
    return await this.#get<TrackGetInfoRes>(
      { autocorrect: "1", ...qs, method: "track.getInfo" },
      "track",
    );
  }

  async track_getSimilar(qs: TrackGetSimilar) {
    if (
      !(
        ((qs as TrackRequest | undefined)?.track &&
          (qs as TrackRequest).artist) ||
        (qs as MBIDTrack | undefined)?.mbid
      )
    ) {
      return LastFm.#sendErr("Missing required params");
    }
    return await this.#get<TrackGetSimilarRes>(
      { autocorrect: "1", ...qs, method: "track.getSimilar" },
      "similartracks",
    );
  }

  async track_getTags(qs: TrackGetTags) {
    if (
      !(
        ((qs as TrackRequest | undefined)?.track &&
          (qs as TrackRequest).artist) ||
        (qs as MBIDTrack | undefined)?.mbid
      )
    ) {
      return LastFm.#sendErr("Missing required params");
    }
    return await this.#get<TrackGetTagsRes>(
      { autocorrect: "1", ...qs, method: "track.getTags" },
      "tags",
    );
  }

  async track_getTopTags(qs: TrackGetTopTags) {
    if (
      !(
        ((qs as TrackRequest | undefined)?.track &&
          (qs as TrackRequest).artist) ||
        (qs as MBIDTrack | undefined)?.mbid
      )
    ) {
      return LastFm.#sendErr("Missing required params");
    }
    return await this.#get<TrackGetTopTagsRes>(
      { autocorrect: "1", ...qs, method: "track.getTopTags" },
      "toptags",
    );
  }

  async track_love(qs: TrackLove | undefined) {
    if (!(qs?.track && qs.artist)) {
      return LastFm.#sendErr("Missing required params");
    }
    return await this.#post({ ...qs, method: "track.love" });
  }

  async track_removeTag(qs: TrackRemoveTag | undefined) {
    if (!(qs?.artist && qs.track && qs.tag)) {
      return LastFm.#sendErr("Missing required params");
    }

    return await this.#post({ ...qs, method: "track.removeTag" });
  }

  async track_scrobble(qs: TrackScrobble | undefined) {
    if (!(qs?.artist && qs.track && qs.timestamp)) {
      return LastFm.#sendErr("Missing required params");
    }

    return await this.#post<TrackScrobbleRes>({
      ...qs,
      method: "track.scrobble",
    });
  }

  async track_search(qs: TrackSearch | undefined) {
    if (!qs?.track) {
      return LastFm.#sendErr("Missing track for search");
    }
    return await this.#get<TrackSearchRes>(
      { autocorrect: "1", ...qs, method: "track.search" },
      "results",
    );
  }

  async track_unlove(qs: TrackUnlove | undefined) {
    if (!(qs?.track && qs.artist)) {
      return LastFm.#sendErr("Missing track or artist");
    }

    return await this.#post({ ...qs, method: "track.unlove" });
  }

  async track_updateNowPlaying(qs: TrackUpdateNowPlaying | undefined) {
    if (!(qs?.track && qs.artist)) {
      return LastFm.#sendErr("Missing track or artist");
    }

    return await this.#post<TrackUpdateNowPlayingRes>({
      ...qs,
      method: "track.updateNowPlaying",
    });
  }

  // User Methods
  async user_getFriends(qs: UserGetFriends | undefined) {
    let localQueryStr = qs;
    if (!localQueryStr?.user) {
      if (!this.#username) {
        return LastFm.#sendErr("Missing user");
      }
      if (!localQueryStr) {
        localQueryStr = { user: this.#username };
      } else {
        localQueryStr.user = this.#username;
      }
    }

    return await this.#get<UserGetFriendsRes>(
      { ...localQueryStr, method: "user.getFriends" },
      "friends",
    );
  }

  async user_getInfo(qs: UserGetInfo | undefined) {
    let localQueryStr = qs;
    if (!localQueryStr?.user) {
      if (!this.#username) {
        return LastFm.#sendErr("Missing user");
      }
      if (!localQueryStr) {
        localQueryStr = { user: this.#username };
      } else {
        localQueryStr.user = this.#username;
      }
    }

    return await this.#get<UserGetInfoRes>(
      { ...localQueryStr, method: "user.getInfo" },
      "user",
    );
  }

  async user_getLovedTracks(qs: UserGetLovedTracks | undefined) {
    let localQueryStr = qs;
    if (!localQueryStr?.user) {
      if (!this.#username) {
        return LastFm.#sendErr("Missing user");
      }
      if (!localQueryStr) {
        localQueryStr = { user: this.#username };
      } else {
        localQueryStr.user = this.#username;
      }
    }

    return await this.#get<UserGetLovedTracksRes>(
      { ...localQueryStr, method: "user.getLovedTracks" },
      "lovedtracks",
    );
  }

  async user_getPersonalTags(qs: UserGetPersonalTags | undefined) {
    if (!qs?.tag) {
      return LastFm.#sendErr("Missing tag");
    }
    if (qs.tag && !qs.user) {
      if (!this.#username) {
        return LastFm.#sendErr("Missing user");
      }

      qs.user = this.#username;
    }

    return await this.#get<UserGetPersonalTagsRes>(
      { ...qs, method: "user.getPersonalTags" },
      "taggings",
    );
  }

  async user_getRecentTracks(qs: UserGetRecentTracks | undefined) {
    let localQueryStr = qs;
    if (!localQueryStr?.user) {
      if (!this.#username) {
        return LastFm.#sendErr("Missing user");
      }
      if (!localQueryStr) {
        localQueryStr = { user: this.#username };
      } else {
        localQueryStr.user = this.#username;
      }
    }

    return await this.#get<UserGetRecentTracksRes>(
      { ...localQueryStr, method: "user.getRecentTracks" },
      "recenttracks",
    );
  }

  async user_getTopAlbums(qs: UserGetTopAlbums | undefined) {
    let localQueryStr = qs;
    if (!localQueryStr?.user) {
      if (!this.#username) {
        return LastFm.#sendErr("Missing user");
      }
      if (!localQueryStr) {
        localQueryStr = { user: this.#username };
      } else {
        localQueryStr.user = this.#username;
      }
    }

    return await this.#get<UserGetTopAlbumsRes>(
      { ...localQueryStr, method: "user.getTopAlbums" },
      "topalbums",
    );
  }

  async user_getTopArtists(qs: UserGetTopArtists | undefined) {
    let localQueryStr = qs;
    if (!localQueryStr?.user) {
      if (!this.#username) {
        return LastFm.#sendErr("Missing user");
      }
      if (!localQueryStr) {
        localQueryStr = { user: this.#username };
      } else {
        localQueryStr.user = this.#username;
      }
    }

    return await this.#get<UserGetTopArtistsRes>(
      { ...localQueryStr, method: "user.getTopArtists" },
      "topartists",
    );
  }

  async user_getTopTags(qs: UserGetTopTags | undefined) {
    let localQueryStr = qs;
    if (!localQueryStr?.user) {
      if (!this.#username) {
        return LastFm.#sendErr("Missing user");
      }
      if (!localQueryStr) {
        localQueryStr = { user: this.#username };
      } else {
        localQueryStr.user = this.#username;
      }
    }

    return await this.#get<UserGetTopTagsRes>(
      { ...localQueryStr, method: "user.getTopTags" },
      "toptags",
    );
  }

  async user_getTopTracks(qs: UserGetTopTracks | undefined) {
    let localQueryStr = qs;
    if (!localQueryStr?.user) {
      if (!this.#username) {
        return LastFm.#sendErr("Missing user");
      }
      if (!localQueryStr) {
        localQueryStr = { user: this.#username };
      } else {
        localQueryStr.user = this.#username;
      }
    }

    return await this.#get<UserGetTopTracksRes>(
      { ...localQueryStr, method: "user.getTopTracks" },
      "toptracks",
    );
  }

  async user_getWeeklyAlbumChart(qs: UserGetWeeklyAlbumChart | undefined) {
    let localQueryStr = qs;
    if (!localQueryStr?.user) {
      if (!this.#username) {
        return LastFm.#sendErr("Missing user");
      }
      if (!localQueryStr) {
        localQueryStr = { user: this.#username };
      } else {
        localQueryStr.user = this.#username;
      }
    }

    return await this.#get<UserGetWeeklyAlbumChartRes>(
      { ...localQueryStr, method: "user.getWeeklyAlbumChart" },
      "weeklyalbumchart",
    );
  }

  async user_getWeeklyArtistChart(qs: UserGetWeeklyArtistChart | undefined) {
    let localQueryStr = qs;
    if (!localQueryStr?.user) {
      if (!this.#username) {
        return LastFm.#sendErr("Missing user");
      }
      if (!localQueryStr) {
        localQueryStr = { user: this.#username };
      } else {
        localQueryStr.user = this.#username;
      }
    }

    return await this.#get<UserGetWeeklyArtistChartRes>(
      { ...localQueryStr, method: "user.getWeeklyArtistChart" },
      "weeklyartistchart",
    );
  }

  async user_getWeeklyChartList(qs: UserGetWeeklyChartList | undefined) {
    let localQueryStr = qs;
    if (!localQueryStr?.user) {
      if (!this.#username) {
        return LastFm.#sendErr("Missing user");
      }
      if (!localQueryStr) {
        localQueryStr = { user: this.#username };
      } else {
        localQueryStr.user = this.#username;
      }
    }

    return await this.#get<UserGetWeeklyChartListRes>(
      { ...localQueryStr, method: "user.getWeeklyChartList" },
      "weeklychartlist",
    );
  }

  async user_getWeeklyTrackChart(qs: UserGetWeeklyTrackChart | undefined) {
    let localQueryStr = qs;
    if (!localQueryStr?.user) {
      if (!this.#username) {
        return LastFm.#sendErr("Missing user");
      }
      if (!localQueryStr) {
        localQueryStr = { user: this.#username };
      } else {
        localQueryStr.user = this.#username;
      }
    }

    return await this.#get<UserGetWeeklyTrackChartRes>(
      { ...localQueryStr, method: "user.getWeeklyTrackChart" },
      "weeklytrackchart",
    );
  }
}

export type {
  LastFmSearch,
  LastFmImage,
  LastFmStats,
  BasicLastFmArtist,
  LastFmArtists,
  LastFmArtist,
  LibraryArtist,
  BasicLastFmTrack,
  LastFmTrack,
  BasicLastFmAlbum,
  LastFmAlbum,
  LastFmTag,
  LastFmAlbums,
  LastFmTracks,
  LastFmTags,
  LastFmWiki,
  LastFmArtistCorrections,
  LastFmRankedArtist,
  LastFmRankedTag,
  LastFmRankedTrack,
  LastFmBoolean,
  LastFmLinks,
  LastFmBio,
} from "~/dataModel/lastFMNatives";

export type {
  AlbumAddTags,
  AlbumGetInfo,
  AlbumGetTags,
  AlbumGetTopTags,
  AlbumInfoRes,
  AlbumRemoveTag,
  AlbumSearch,
  AlbumSearchRes,
  AlbumTagsRes,
  MBIDAlbum,
  NamedAlbum,
} from "~/dataModel/albumFunctions";

// biome-ignore lint/performance/noBarrelFile: Needed for TS enum export
export { LastFmImageSize, Period } from "~/dataModel/lastFMNatives";

export type {
  ArtistAddTags,
  ArtistGetCorrection,
  ArtistGetCorrectionRes,
  ArtistGetInfo,
  ArtistGetInfoRes,
  ArtistGetSimilar,
  ArtistGetSimilarRes,
  ArtistGetTags,
  ArtistGetTagsRes,
  ArtistGetTopAlbums,
  ArtistGetTopAlbumsRes,
  ArtistGetTopTags,
  ArtistGetTopTagsRes,
  ArtistGetTopTracks,
  ArtistGetTopTracksRes,
  ArtistRemoveTag,
  ArtistSearch,
  ArtistSearchRes,
  MBIDArtist,
  NamedArtist,
} from "~/dataModel/artistFunctions";
export type {
  ChartGetTopArtists,
  ChartGetTopTags,
  ChartGetTopTracks,
  ChartTopTagsRes,
  ChartTopTracksRes,
} from "~/dataModel/chartFunctions";
export type {
  GeoArtistsRequest,
  GeoTopArtistsRes,
  GeoTopTracksRes,
  GeoTracksRequest,
} from "~/dataModel/geoFunctions";
export type {
  LibraryRequest,
  LibraryArtistsRes,
} from "~/dataModel/libraryFunctions";
export type {
  ErrorMessage,
  ErrorResponse,
  LastFmConstructor,
  LastFmRequest,
  PostRequest,
  ResponseData,
} from "~/dataModel/sharedFunctions";
export type {
  TagInfoRequest,
  TagInfoRes,
  TagSimilarRequest,
  TagSimilarRes,
  TagTopAlbumsRes,
  TagTopArtistsRes,
  TagTopRequest,
  TagTopTagsRequest,
  TagTopTracksRes,
  TagWeeklyChartRes,
  TopTagsRes,
} from "~/dataModel/tagFunctions";
export type {
  MBIDTrack,
  TrackAddTags,
  TrackGetCorrectionRequest,
  TrackGetCorrectionRes,
  TrackGetInfo,
  TrackGetInfoRes,
  TrackGetSimilar,
  TrackGetSimilarRes,
  TrackGetTags,
  TrackGetTagsRes,
  TrackGetTopTags,
  TrackGetTopTagsRes,
  TrackLove,
  TrackRemoveTag,
  TrackRequest,
  TrackScrobble,
  TrackScrobbleRes,
  TrackSearch,
  TrackSearchRes,
  TrackUnlove,
  TrackUpdateNowPlaying,
  TrackUpdateNowPlayingRes,
} from "~/dataModel/trackFunctions";
export type {
  UserGetFriends,
  UserGetFriendsRes,
  UserGetInfo,
  UserGetInfoRes,
  UserGetLovedTracks,
  UserGetLovedTracksRes,
  UserGetPersonalTags,
  UserGetPersonalTagsRes,
  UserGetRecentTracks,
  UserGetRecentTracksRes,
  UserGetTopAlbums,
  UserGetTopAlbumsRes,
  UserGetTopArtists,
  UserGetTopArtistsRes,
  UserGetTopTags,
  UserGetTopTagsRes,
  UserGetTopTracks,
  UserGetTopTracksRes,
  UserGetWeeklyAlbumChart,
  UserGetWeeklyAlbumChartRes,
  UserGetWeeklyArtistChart,
  UserGetWeeklyArtistChartRes,
  UserGetWeeklyChartList,
  UserGetWeeklyChartListRes,
  UserGetWeeklyTrackChart,
  UserGetWeeklyTrackChartRes,
} from "~/dataModel/userFunctions";

export { SearchType } from "~/dataModel/searchFunctions";
export type { Search } from "~/dataModel/searchFunctions";
