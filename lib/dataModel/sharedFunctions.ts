export type ErrorMessage = {
  error: number;
  message: string;
  success: false;
};

export type ErrorResponse = {
  error: Error;
  success: false;
};

export type LastFmConstructor = {
  debug?: boolean;
  apiKey?: string;
  apiSecret?: string;
  username?: string;
  password?: string;
  sessionKey?: string;
};

export type ResponseData<R = Record<string, unknown>> = R & {
  success: true;
};

export type LastFmRequest = {
  method: string;
  autocorrect?: string;
  username?: string;
};

export type PostRequest = {
  api_key: string;
  format: string;
  sk: string;
  api_sig: string;
} & LastFmRequest;
