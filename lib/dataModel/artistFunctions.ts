import type {
  LastFmAlbums,
  LastFmArtist,
  LastFmArtistCorrections,
  LastFmArtists,
  LastFmSearch,
  LastFmTags,
  LastFmTracks,
} from "~/dataModel/lastFMNatives";
import { type Search, SearchType } from "~/dataModel/searchFunctions";
import type { LastFmRequest } from "~/dataModel/sharedFunctions";

type ArtistRequest = {
  artist: string;
} & Omit<LastFmRequest, "method">;

export type MBIDArtist = Omit<ArtistRequest, "method" | "artist"> & {
  mbid: string;
};
export type NamedArtist = ArtistRequest;
type ArtistGet = MBIDArtist | NamedArtist;

export type ArtistGetInfoRes = LastFmArtist;
export type ArtistGetSimilarRes = LastFmArtists;
export type ArtistGetCorrectionRes = {
  correction: LastFmArtistCorrections;
};
export type ArtistGetTagsRes = {
  "@attr": {
    artist: string;
  };
} & LastFmTags;
export type ArtistGetTopAlbumsRes = {
  "@attr": {
    artist: string;
    page: string;
    perPage: string;
    totalPages: string;
    total: string;
  };
} & LastFmAlbums;
export type ArtistGetTopTagsRes = {
  "@attr": {
    artist: string;
  };
} & LastFmTags;
export type ArtistGetTopTracksRes = {
  "@attr": {
    artist: string;
    page: string;
    perPage: string;
    totalPages: string;
    total: string;
  };
} & LastFmTracks;
export type ArtistSearchRes = {
  artistmatches: LastFmArtists;
} & LastFmSearch;

export type ArtistAddTags = {
  tags: string;
} & ArtistRequest;
export type ArtistRemoveTag = {
  tag: string;
} & ArtistRequest;
export type ArtistGetCorrection = NamedArtist;
export type ArtistGetInfo = ArtistGet & { username?: string };
export type ArtistGetSimilar = ArtistGet;
export type ArtistGetTags = ArtistGet & { user?: string };
export type ArtistGetTopAlbums = ArtistGet;
export type ArtistGetTopTags = ArtistGet;
export type ArtistGetTopTracks = ArtistGet;

export type ArtistSearch = Search<{ [SearchType.ARTIST]: string }>;
