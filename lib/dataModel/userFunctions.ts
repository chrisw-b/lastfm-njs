import type {
  BasicLastFmArtist,
  BasicLastFmTrack,
  LastFmBoolean,
  LastFmImage,
  LastFmTag,
  Period,
} from "~/dataModel/lastFMNatives";
import type { LastFmRequest } from "~/dataModel/sharedFunctions";

type UserRequest = {
  user: string;
} & Omit<LastFmRequest, "method">;

export type UserGetFriends = {
  recenttracks?: boolean;
  limit?: number;
  page?: number;
} & UserRequest;
export type LastFmFriend = {
  name: string;
  url: string;
  country: string;
  playlists: string;
  playcount: string;
  image: LastFmImage[];
  registered: {
    unixtime: string;
    "#text": string;
  };
  realname: string;
  subscriber: LastFmBoolean;
  bootstrap: LastFmBoolean;
  type: "subscriber" | "alum" | "user";
};
export type UserGetFriendsRes = {
  "@attr": {
    user: string;
    totalPages: string;
    page: string;
    perPage: string;
    total: string;
  };
  user: LastFmFriend[];
};

export type UserGetInfo = {
  user?: string;
} & Omit<LastFmRequest, "method">;
export type UserGetInfoRes = LastFmFriend;

export type UserGetLovedTracks = {
  limit?: number;
  page?: number;
} & UserRequest;
export type UserGetLovedTracksRes = {
  track: {
    artist: BasicLastFmArtist;
    date: {
      unixtime: string;
      "#text": string;
    };
    mbid: string;
    url: string;
    name: string;
    image: LastFmImage[];
    streamable: {
      fulltrack: LastFmBoolean;
      "#text": LastFmBoolean;
    };
  };
  "@attr": {
    user: string;
    totalPages: string;
    page: string;
    perPage: string;
    total: string;
  };
};

export type UserGetPersonalTags = {
  user: string;
  tag: string;
  taggingtype: "artist" | "album" | "track";
  limit?: number;
  page?: number;
} & UserRequest;
export type UserGetPersonalTagsRes = {
  artists: {
    artist: BasicLastFmArtist[];
  };
  "@attr": {
    user: string;
    tag: string;
    totalPages: string;
    page: string;
    perPage: string;
    total: string;
  };
};

export type UserGetRecentTracks = {
  limit?: number;
  page?: number;
  from?: string;
  extended?: LastFmBoolean;
  to?: string;
} & UserRequest;
type RecentTrack = {
  date: {
    unixtime: string;
    "#text": string;
  };
  image: LastFmImage[];
  url: string;
  streamable: LastFmBoolean;
  loved: LastFmBoolean;
  album: {
    mbid: string;
    "#text": string;
  };
  artist: {
    mbid: string;
    "#text": string;
  };
} & Omit<BasicLastFmTrack, "artist">;
export type UserGetRecentTracksRes = {
  track: RecentTrack[];
  "@attr": {
    user: string;
    totalPages: string;
    page: string;
    perPage: string;
    total: string;
  };
};

type TopRequest = {
  period?: Period;
  limit?: number;
  page?: number;
} & UserRequest;

type UserRankedItem = {
  mbid: string;
  url: string;
  playcount: string;
  name: string;
  "@attr": {
    rank: string;
  };
};

export type UserGetTopAlbums = TopRequest;
type TopAlbum = {
  artist: BasicLastFmArtist;
  image: LastFmImage[];
} & UserRankedItem;
export type UserGetTopAlbumsRes = {
  album: TopAlbum[];
  "@attr": {
    user: string;
    totalPages: string;
    page: string;
    perPage: string;
    total: string;
  };
};

export type UserGetTopArtists = TopRequest;
type TopArtist = {
  image: LastFmImage[];
} & UserRankedItem;
export type UserGetTopArtistsRes = {
  artist: TopArtist[];
  "@attr": {
    user: string;
    totalPages: string;
    page: string;
    perPage: string;
    total: string;
  };
};

export type UserGetTopTags = {
  limit?: number;
} & TopRequest;
export type UserGetTopTagsRes = {
  tag: LastFmTag;
  "@attr": {
    user: string;
  };
};

export type UserGetTopTracks = TopRequest;
type TopTrack = {
  image: LastFmImage[];
  artist: BasicLastFmArtist;
  duration: string;
  streamable: {
    fulltrack: LastFmBoolean;
    "#text": LastFmBoolean;
  };
} & UserRankedItem;
export type UserGetTopTracksRes = {
  track: TopTrack[];
  "@attr": {
    user: string;
    totalPages: string;
    page: string;
    perPage: string;
    total: string;
  };
};

type WeeklyChartRes = {
  "@attr": {
    user: string;
    from: string;
    to: string;
  };
};

export type UserGetWeeklyAlbumChart = {
  from?: string;
  to?: string;
} & TopRequest;
type ChartedAlbum = {
  artist: BasicLastFmArtist;
} & UserRankedItem;
export type UserGetWeeklyAlbumChartRes = {
  album: ChartedAlbum[];
} & WeeklyChartRes;

export type UserGetWeeklyArtistChart = {
  from?: string;
  to?: string;
} & TopRequest;
export type UserGetWeeklyArtistChartRes = {
  album: UserRankedItem[];
} & WeeklyChartRes;

export type UserGetWeeklyChartList = TopRequest;
type ChartItem = {
  from: string;
  to: string;
  "#text": string;
};
export type UserGetWeeklyChartListRes = {
  chart: ChartItem[];
  "@attr": {
    user: string;
  };
};

export type UserGetWeeklyTrackChart = {
  from?: string;
  to?: string;
} & TopRequest;
type ChartedTrack = {
  artist: BasicLastFmArtist;
  image: LastFmImage[];
} & UserRankedItem;
export type UserGetWeeklyTrackChartRes = {
  track: ChartedTrack[];
} & WeeklyChartRes;
