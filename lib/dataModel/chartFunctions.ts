import type {
  LastFmRankedArtist,
  LastFmRankedTag,
  LastFmRankedTrack,
} from "~/dataModel/lastFMNatives";
import type { LastFmRequest } from "~/dataModel/sharedFunctions";

export type ChartRequest = {
  page?: number;
  limit?: number;
} & Omit<LastFmRequest, "method">;

export type ChartTopTracksRes = {
  tracks: LastFmRankedTrack[];
  "@attr": {
    page: string;
    perPage: string;
    totalPages: string;
    total: string;
  };
};
export type ChartTopTagsRes = {
  tags: LastFmRankedTag[];
  "@attr": {
    page: string;
    perPage: string;
    totalPages: string;
    total: string;
  };
};
export type ChartTopArtistsRes = {
  artists: LastFmRankedArtist[];
  "@attr": {
    page: string;
    perPage: string;
    totalPages: string;
    total: string;
  };
};
export type ChartGetTopArtists = ChartRequest;
export type ChartGetTopTracks = ChartRequest;
export type ChartGetTopTags = ChartRequest;
