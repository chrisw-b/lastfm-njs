import type {
  LastFmAlbum,
  LastFmAlbums,
  LastFmSearch,
  LastFmTags,
} from "~/dataModel/lastFMNatives";
import { type Search, SearchType } from "~/dataModel/searchFunctions";
import type { LastFmRequest } from "~/dataModel/sharedFunctions";

type AlbumRequest = {
  artist: string;
  album: string;
  user?: string;
} & Omit<LastFmRequest, "method">;

export type AlbumInfoRes = LastFmAlbum;
export type AlbumTagsRes = {
  "@attr": {
    artist: string;
    album: string;
  };
} & LastFmTags;

export type MBIDAlbum = Omit<AlbumRequest, "method" | "artist" | "album"> & {
  mbid: string;
};
export type NamedAlbum = AlbumRequest;
type AlbumGet = MBIDAlbum | NamedAlbum;

export type AlbumSearchRes = {
  albummatches: LastFmAlbums;
} & LastFmSearch;

export type AlbumAddTags = {
  tags: string;
} & AlbumRequest;
export type AlbumRemoveTag = {
  tag: string;
} & AlbumRequest;

export type AlbumGetTopTags = AlbumGet;
export type AlbumGetTags = AlbumGet & { user?: string };
export type AlbumGetInfo = AlbumGet & { username?: string };
export type AlbumSearch = Search<{ [SearchType.ALBUM]: string }>;
