import type { LibraryArtist } from "~/dataModel/lastFMNatives";
import type { LastFmRequest } from "~/dataModel/sharedFunctions";

export type LibraryRequest = {
  user: string;
  page?: number;
  limit?: number;
} & Omit<LastFmRequest, "method">;

export type LibraryArtistsRes = {
  artists: LibraryArtist[];
  "@attr": {
    user: string;
    page: string;
    perPage: string;
    totalPages: string;
    total: string;
  };
};
