import type {
  LastFmRankedArtist,
  LastFmRankedTrack,
} from "~/dataModel/lastFMNatives";
import type { LastFmRequest } from "~/dataModel/sharedFunctions";

export type GeoArtistsRequest = {
  country: string;
  page?: number;
  limit?: number;
} & Omit<LastFmRequest, "method">;

export type GeoTracksRequest = {
  location?: string;
} & GeoArtistsRequest;

export type GeoTopArtistsRes = {
  artists: LastFmRankedArtist[];
  "@attr": {
    country: string;
    page: string;
    perPage: string;
    totalPages: string;
    total: string;
  };
};

export type GeoTopTracksRes = {
  tracks: LastFmRankedTrack[];
  "@attr": {
    country: string;
    page: string;
    perPage: string;
    totalPages: string;
    total: string;
  };
};
