export type LastFmSearch = {
  "opensearch:Query": {
    "#text": string;
    role: string;
    searchTerms?: string;
    startPage: string;
  };
  "opensearch:totalResults": string;
  "opensearch:startIndex": string;
  "opensearch:itemsPerPage": string;
  "@attr": {
    for?: string;
  };
};

export enum LastFmImageSize {
  SMALL = "small",
  MEDIUM = "medium",
  LARGE = "large",
  EXTRALARGE = "extralarge",
  MEGA = "mega",
}

export type LastFmImage = {
  "#text": string;
  size: LastFmImageSize;
};
export type LastFmStats = {
  listeners: string;
  playcount: string;
};

export type BasicLastFmArtist = {
  name: string;
  mbid: string;
  url: string;
  image?: LastFmImage[];
};
export type LastFmLinks = {
  link: {
    "#text": string;
    rel: string;
    href: string;
  };
};
export type LastFmArtists = {
  artist: BasicLastFmArtist[];
};

export type LastFmBio = {
  links: LastFmLinks;
  published: string;
  summary: string;
  content: string;
};
export type LastFmArtist = {
  streamable: LastFmBoolean;
  ontour: string;
  stats: LastFmStats;
  similar: LastFmArtists;
  tags: LastFmTags;
  bio?: LastFmBio;
} & BasicLastFmArtist;
export type LibraryArtist = {
  tagcount: string;
  playcount: string;
  streamable: LastFmBoolean;
} & BasicLastFmArtist;
export type BasicLastFmTrack = {
  name: string;
  url: string;
  artist: LastFmArtist;
  mbid: string;
};
export type LastFmTrack = {
  duration: string;
  "@attr": {
    rank: string;
  };
  streamable: {
    "#text": string;
    fulltrack: LastFmBoolean;
  };
} & BasicLastFmTrack;

export type BasicLastFmAlbum = {
  name: string;
  artist: string;
  mbid: string;
  url?: string;
  image?: LastFmImage[];
};

export type LastFmAlbum = {
  tracks?: LastFmTracks;
  wiki?: LastFmWiki;
} & BasicLastFmAlbum &
  LastFmStats;

export type LastFmTag = {
  name: string;
  url: string;
  count?: number;
};

export type LastFmAlbums = {
  album: LastFmAlbum[];
};

export type LastFmTracks = {
  track: LastFmTrack[];
};
export type LastFmTags = {
  tag: LastFmTag[];
};
export type LastFmWiki = {
  published?: string;
  summary: string;
  content: string;
};
export type LastFmArtistCorrections = {
  artist: BasicLastFmArtist;
  "@attr": {
    index: string;
  };
};

export type LastFmRankedArtist = {
  name: string;
  mbid: string;
  url: string;
  streamable: LastFmBoolean;
  image: LastFmImage[];
  "@attr": {
    rank: string;
  };
} & LastFmStats;

export type LastFmRankedTag = {
  name: string;
  url: string;
  reach: number;
  taggings: number;
  streamable: number;
  wiki: Record<string, string>;
  "@attr": {
    rank: string;
  };
};

export type LastFmRankedTrack = {
  streamable: {
    "#text": string;
    fulltrack: LastFmBoolean;
  };
  artist: BasicLastFmArtist;
  "@attr": {
    rank: string;
  };
} & LastFmStats &
  Omit<BasicLastFmTrack, "artist">;

export enum Period {
  OVERALL = "overall",
  WEEK = "7day",
  MONTH = "1month",
  THREE_MONTH = "3month",
  SIX_MONTH = "6month",
  YEAR = "12month",
}

export type LastFmBoolean = "0" | "1";
