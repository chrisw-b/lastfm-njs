import type {
  BasicLastFmAlbum,
  BasicLastFmArtist,
  BasicLastFmTrack,
  LastFmRankedTag,
  LastFmSearch,
  LastFmStats,
  LastFmTag,
  LastFmTrack,
  LastFmTracks,
  LastFmWiki,
} from "~/dataModel/lastFMNatives";
import { type Search, SearchType } from "~/dataModel/searchFunctions";
import type { LastFmRequest } from "~/dataModel/sharedFunctions";

export type TrackRequest = {
  artist: string;
  track: string;
} & Omit<LastFmRequest, "method">;

export type MBIDTrack = Omit<TrackRequest, "artist" | "track"> & {
  mbid: string;
};
export type NamedTrack = TrackRequest;
type TrackGet = MBIDTrack | NamedTrack;

export type TrackAddTags = {
  tags: string;
} & TrackRequest;

export type TrackGetCorrectionRequest = Omit<TrackRequest, "method">;
export type TrackGetCorrectionRes = {
  correction: {
    track: BasicLastFmTrack;
    "@attr": {
      index: string;
      artistcorrected: string;
      trackcorrected: string;
    };
  };
};

export type TrackGetInfo = TrackGet & { username?: string };
export type TrackGetInfoRes = {
  artist: BasicLastFmArtist;
  album: BasicLastFmAlbum & { "@attr": { position: string } };
  toptags: {
    tag: LastFmTag;
  };
  wiki: LastFmWiki;
} & Omit<LastFmTrack, "artist" | "@attr"> &
  LastFmStats;

export type TrackGetSimilar = TrackGet & { limit?: number };

type SimilarTrack = {
  match: number;
  mbid: string;
  duration: number;
} & Omit<LastFmTrack, "artist" | "@attr" | "duration">;
export type TrackGetSimilarRes = {
  track: SimilarTrack[];
};

export type TrackGetTags = TrackGet & { user?: string };
export type TrackGetTagsRes = {
  tag: LastFmTag[];
  "@attr": {
    artist: string;
    track: string;
  };
};

export type TrackGetTopTags = TrackGet;
export type TrackGetTopTagsRes = {
  tag: LastFmRankedTag[];
  "@attr": {
    artist: string;
    track: string;
  };
};

export type TrackLove = TrackRequest;
export type TrackRemoveTag = TrackRequest & { tag: string };

export type TrackScrobble = {
  timestamp: number;
  album?: string;
  streamId?: string;
  chosenByUser?: "1" | "0";
  trackNumber?: number;
  mbid?: string;
  albumArtist?: string;
  duration?: string;
} & TrackRequest;
type AudioScrobblerScrobbleEntry = {
  corrected: string;
  "#text"?: string;
};
type ScrobbleData = {
  artist: AudioScrobblerScrobbleEntry;
  ignoredMessage: {
    code: string;
    message: string;
  };
  albumArtist: AudioScrobblerScrobbleEntry;
  timestamp: string;
  album: AudioScrobblerScrobbleEntry;
  track: AudioScrobblerScrobbleEntry;
};
export type TrackScrobbleRes = {
  scrobble: {
    scrobbles: ScrobbleData[];
  };
  "@attr": {
    accepted: string;
    ignored: string;
  };
};

export type TrackSearch = Search<{ [SearchType.TRACK]: string }>;
export type TrackSearchRes = {
  trackmatches: LastFmTracks;
} & LastFmSearch;

export type TrackUnlove = TrackLove;

export type TrackUpdateNowPlaying = {
  album?: string;
  trackNumber?: string;
  mbid?: string;
  duration?: string;
  albumArtist?: string;
} & TrackRequest;
export type TrackUpdateNowPlayingRes = {
  nowPlaying: ScrobbleData;
};
