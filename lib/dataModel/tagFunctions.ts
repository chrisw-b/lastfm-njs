import type { ChartRequest } from "~/dataModel/chartFunctions";
import type {
  BasicLastFmArtist,
  LastFmAlbum,
  LastFmBoolean,
  LastFmRankedTrack,
  LastFmWiki,
} from "~/dataModel/lastFMNatives";
import type { LastFmRequest } from "~/dataModel/sharedFunctions";

type TagRequest = {
  tag: string;
} & Omit<LastFmRequest, "method">;

export type TagInfoRequest = {
  lang?: string;
  tag: string;
} & TagRequest;

export type TagInfoRes = {
  name: string;
  total: number;
  reach: number;
  streamable?: string;
  url?: string;
  wiki: LastFmWiki;
};

export type TagSimilarRequest = {
  tag: string;
} & TagRequest;

type SimilarTag = {
  name: string;
  url: string;
  streamable: LastFmBoolean;
};
export type TagSimilarRes = {
  tag: SimilarTag[];
  "@attr": {
    tag: string;
  };
};

export type TagTopRequest = TagRequest & ChartRequest;

type TagAlbum = {
  artist: BasicLastFmArtist;
  "@attr": {
    rank: string;
  };
} & Omit<LastFmAlbum, "artist">;

export type TagTopAlbumsRes = {
  album: TagAlbum[];
  "@attr": {
    tag: string;
    page: string;
    perPage: string;
    totalPages: string;
    total: string;
  };
};

type TagArtist = {
  streamable: LastFmBoolean;
  "@attr": {
    rank: string;
  };
} & BasicLastFmArtist;

export type TagTopArtistsRes = {
  artist: TagArtist[];
  "@attr": {
    tag: string;
    page: string;
    perPage: string;
    totalPages: string;
    total: string;
  };
};

type TagTrack = {
  "@attr": {
    rank: string;
  };
} & LastFmRankedTrack;

export type TagTopTracksRes = {
  track: TagTrack[];
  "@attr": {
    tag: string;
    page: string;
    perPage: string;
    totalPages: string;
    total: string;
  };
};

export type TagTopTagsRequest = ChartRequest;

type RankedTag = {
  name: string;
  count: number;
  reach: number;
};

export type TopTagsRes = {
  tag: RankedTag[];
  "@attr": {
    offset: number;
    num_res: number;
    total: number;
  };
};

export type TagWeeklyChartRes = {
  chart: {
    "#text": string;
    from: string;
    to: string;
  };
  "@attr": {
    tag: string;
  };
};
