export enum SearchType {
  ALBUM = "album",
  ARTIST = "artist",
  TRACK = "track",
}

export type Search<T extends Partial<Record<SearchType, string>>> = T & {
  limit?: number;
  page?: number;
};
