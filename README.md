# LastFM-njs

A fully featured interface for Node and the Last.FM api

The full Last.FM API description can be found [here](http://www.last.fm/api)

You'll need an API key from [Create API Account](http://www.last.fm/api/account/create)

You can install using `npm install --save lastfm-njs`

Then you can set up like so, where username is a default username:

```ts
var lastfm = require("lastfm-njs");
var config = require("./config");
var lfm = new lastfm.default({
  apiKey: config.key,
  apiSecret: config.secret,
  username: config.username,
});
```

If a method requires writing to an account, then you also need a password

```ts
var lfm = new lastfm.default({
  apiKey: config.key,
  apiSecret: config.secret,
  username: config.username,
  password: config.password,
});
```

This library supports both esm and cjs importing, commonjs will export as a default. You can use modules like

```ts
import LastFM from 'lastfm-njs';

const client = new LastFM(...)
```

or require with

```ts
const LastFM = require("lastfm-njs");

const client = new LastFM.default(...)
```

After this, you can use any of the methods

## Documentation

- [Authentication Methods](#authentication-methods)
- [Album Methods](#album-methods)
- [Artist Methods](#artist-methods)
- [Geo Methods](#geo-methods)
- [Library Methods](#library-methods)
- [Tag Methods](#tag-methods)
- [Track Methods](#track-methods)
- [User Methods](#user-methods)

#### All methods use promises

```ts
lastfm
  .auth_getMobileSession()
  .then(function (result) {
    lastfm
      .album_addTags({
        artist: "Oh Pep!",
        album: "Living",
        tags: "peppy,folk,music",
      })
      .then(printRes)
      .catch(printError);
  })
  .catch(printError);
```

See example files for other examples

### Authentication

[Last.FM Documentation](http://www.last.fm/api/show/auth.getMobileSession)

**A username and password is required**

`auth_getMobileSession();`

which either returns

```ts
{
    success: true,
    key: 'XXX'
}
```

or

```ts
{
    success: false,
    error: [lastFMError]
}
```

### Album Methods

[Examples](../master/examples/album_examples.ts)

#### Add Tags\*

[Last.FM Documentation](http://www.last.fm/api/show/album.addTags)

`album_addTags(opt)`, where

```ts
opt = {
  artist: artist, //req
  album: album, //req
  tags: tags, //req, max: 10, comma separated list
};
```

_\*Requires Authentication_

#### Get Info

[Last.FM Documentation](https://www.last.fm/api/show/album.getInfo)

`album_getInfo(opt)`, where

```ts
opt = {
  artist: artist, //req unless mbid
  album: album, //req unless mbid
  mbid: mbid, //opt
  lang: lang, //opt
  username: username, //opt
};
```

#### Get Tags

[Last.FM Documentation](https://www.last.fm/api/show/album.getTags)

`album_getTags(opt)`, where

```ts
opt = {
  artist: artist, //req unless mbid
  album: album, //req unless mbid
  username: username, //req
  mbid: mbid, //opt
};
```

#### Get Top Tags

[Last.FM Documentation](https://www.last.fm/api/show/album.getTopTags)

`album_getTopTags(opt)`, where

```ts
opt = {
  artist: artist, //req unless mbid
  album: album, //req unless mbid
  mbid: mbid, //opt
};
```

#### Remove Tag\*

[Last.FM Documentation](http://www.last.fm/api/show/album.removeTag)

`album_removeTag(opt)`, where

```ts
opt = {
  artist: artist, //req
  album: album, //req
  tag: tag, //req, single tag to remove
};
```

_\*Requires Authentication_

#### Album Search

[Last.FM Documentation](https://www.last.fm/api/show/album.search)

`album_getTopTags(opt)`, where

```ts
opt = {
  album: album, //req
  limit: limit, //opt, defaults to 30
  page: page, //opt, defaults to 1
};
```

## Artist

[Examples](../master/examples/artist_examples.ts)

#### Add Tags\*

[Last.FM Documentation](http://www.last.fm/api/show/artist.addTags)

`artist_addTags(opt)`, where

```ts
    opt = {
        artist: artist //req
        tags: tags, //req, max 10, comma separated
    }
```

_\*Requires Authentication_

#### Get Correction

[Last.FM Documentation](http://www.last.fm/api/show/artist.getCorrection)

`artist_getCorrection(opt)`, where

```ts
opt = {
  artist: artist, //req
};
```

#### Get Info

[Last.FM Documentation](http://www.last.fm/api/show/artist.getInfo)

`artist_getInfo(opt)`, where

```ts
opt = {
  artist: artist, //req unless mbid
  mbid: mbid, //opt
  username: username, //opt
};
```

#### Get Similar

[Last.FM Documentation](http://www.last.fm/api/show/artist.getSimilar)

`artist_getSimilar(opt)`, where

```ts
opt = {
  artist: artist, //req unless mbid
  mbid: mbid, //opt
  limit: limit, //opt
};
```

#### Get Tags

[Last.FM Documentation](http://www.last.fm/api/show/artist.getTags)

`artist_getTags(opt)`, where

```ts
opt = {
  artist: artist, //req unless mbid
  user: username, //req
  mbid: mbid, //opt
};
```

#### Get Top Albums

[Last.FM Documentation](http://www.last.fm/api/show/artist.getTopAlbums)

`artist_getTopAlbums(opt)`, where

```ts
opt = {
  artist: artist, //req unless mbid
  mbid: mbid, //opt
  page: page, //opt, default is 50
  limit: limit, //opt, default is 1
};
```

#### Get Top Tags

[Last.FM Documentation](http://www.last.fm/api/show/artist.getTopTags)

`artist_getTopTags(opt)`, where

```ts
opt = {
  artist: artist, //req unless mbid
  mbid: mbid, //opt
};
```

#### Get Top Tracks

[Last.FM Documentation](http://www.last.fm/api/show/artist.getTopTracks)

`artist_getTopTracks(opt)`, where

```ts
opt = {
  artist: artist, //req unless mbid
  mbid: mbid, //opt
  page: page, //opt, default is 1
  limit: limit, //opt, default is 50
};
```

#### Remove Tag\*

[Last.FM Documentation](http://www.last.fm/api/show/artist.removeTag)

`artist_removeTag(opt)`, where

```ts
    opt = {
        artist: artist //req
        tag: tag, //req, 1 tag to be removed
    }
```

_\*Requires Authentication_

#### Search

[Last.FM Documentation](http://www.last.fm/api/show/artist.search)

`artist_search(opt)`, where

```ts
opt = {
  artist: artist, //req unless mbid
  page: page, //opt, default is 1
  limit: limit, //opt, default is 50
};
```

### Chart Methods

[Examples](../master/examples/chart_examples.ts)

#### Get Top Artists

[Last.FM Documentation](http://www.last.fm/api/show/chart.getTopArtists)

`chart_getTopArtists(opt)`, where

```ts
opt = {
  page: page, //opt, default is 1
  limit: limit, //opt, default is 50
};
```

#### Get Top Tags

[Last.FM Documentation](http://www.last.fm/api/show/chart.getTopTags)

`chart_getTopTags(opt)`, where

```ts
opt = {
  page: page, //opt, default is 1
  limit: limit, //opt, default is 50
};
```

#### Get Top Tracks

[Last.FM Documentation](http://www.last.fm/api/show/chart.getTopTracks)

`chart_getTopTracks(opt)`, where

```ts
opt = {
  page: page, //opt, default is 1
  limit: limit, //opt, default is 50
};
```

### Geo Methods

[Examples](../master/examples/geo_examples.ts)

#### Get Top Artists

[Last.FM Documentation](http://www.last.fm/api/show/geo.getTopArtists)

`geo_getTopArtists(opt)`, where

```ts
opt = {
  country: country, //req, ISO 3166-1 format
  page: page, //opt, default is 1
  limit: limit, //opt, default is 50
};
```

#### Get Top Tracks

[Last.FM Documentation](http://www.last.fm/api/show/geo.getTopTracks)

`geo_getTopTracks(opt)`, where

```ts
opt = {
  country: country, //req, ISO 3166-1 format
  page: page, //opt, default is 1
  limit: limit, //opt, default is 50
};
```

### Library Methods

[Examples](../master/examples/library_examples.ts)

#### Get Artists

[Last.FM Documentation](http://www.last.fm/api/show/library.getArtists)

`library_getArtists(opt)`, where

```ts
opt = {
  user: username, //req
  page: page, //opt, default is 1
  limit: limit, //opt, default is 50
};
```

### Tag Methods

[Examples](../master/examples/library_examples.ts)

#### Get Info

[Last.FM Documentation](http://www.last.fm/api/show/tag.getInfo)

`tag_getInfo(opt)`, where

```ts
opt = {
  tag: tag, //req
  lang: lang, //opt
};
```

#### Get Similar

[Last.FM Documentation](http://www.last.fm/api/show/tag.getSimilar)

`tag_getSimilar(opt)`, where

```ts
opt = {
  tag: tag, //req
};
```

#### Get Top Albums

[Last.FM Documentation](http://www.last.fm/api/show/tag.getTopAlbums)

`tag_getTopAlbums(opt)`, where

```ts
opt = {
  tag: tag, //req
  limit: limit, //opt, default is 50
  page: page, //opt, default is 1
};
```

#### Get Top Artists

[Last.FM Documentation](http://www.last.fm/api/show/tag.getTopArtists)

`tag_getTopArtists(opt)`, where

```ts
opt = {
  tag: tag, //req
  limit: limit, //opt, default is 50
  page: page, //opt, default is 1
};
```

#### Get Top Tags

[Last.FM Documentation](http://www.last.fm/api/show/tag.getTopTags)

`tag_getTopTags(opt)`, where

```ts
opt = {};
```

#### Get Top Tracks

[Last.FM Documentation](http://www.last.fm/api/show/tag.getTopTracks)

`tag_getTopTracks(opt)`, where

```ts
opt = {
  tag: tag, //req
  limit: limit, //opt, defaults to 50
  page: page, //opt, defaults to 1
};
```

#### Get Weekly Chart List

[Last.FM Documentation](http://www.last.fm/api/show/tag.getWeeklyChartList)

`tag_getWeeklyChartList(opt)`, where

```ts
opt = {
  tag: tag, //req
};
```

### Track Methods

[Examples](../master/examples/track_examples.ts)

#### Add Tags\*

[Last.FM Documentation](http://www.last.fm/api/show/track.addTags)

`track_addTags(opt)`, where

```ts
opt = {
  artist: artist, //req
  track: track, //req
  tags: tags, //req, max: 10
};
```

_\*Requires Authentication_

#### Get Correction

[Last.FM Documentation](http://www.last.fm/api/show/track.getCorrection)

`track_getCorrection(opt)`, where

```ts
opt = {
  artist: artist, //req
  track: track, //req
};
```

#### Get Info

[Last.FM Documentation](http://www.last.fm/api/show/track.getInfo)

`track_getInfo(opt)`, where

```ts
opt = {
  artist: artist, //req unless mbid
  track: track, //req unless mbid
  mbid: mbid, //opt
  username: username, //opt
};
```

#### Get Similar

[Last.FM Documentation](http://www.last.fm/api/show/track.getSimilar)

`track_getSimilar(opt)`, where

```ts
opt = {
  artist: artist, //req unless mbid
  track: track, //req unless mbid
  mbid: mbid, //opt
  limit: limit, //opt
};
```

#### Get Tags

[Last.FM Documentation](http://www.last.fm/api/show/track.getTags)

`track_getTags(opt)`, where

```ts
opt = {
  artist: artist, //req unless mbid
  track: track, //req unless mbid
  username: username, //req
  mbid: mbid, //opt
};
```

#### Get Top Tags

[Last.FM Documentation](http://www.last.fm/api/show/track.getTopTags)

`track_getTopTags(opt)`, where

```ts
opt = {
  artist: artist, //req unless mbid
  track: track, //req unless mbid
  mbid: mbid, //opt
};
```

#### Love Track\*

[Last.FM Documentation](http://www.last.fm/api/show/track.love)

`track_love(opt)`, where

```ts
opt = {
  artist: artist, //req unless mbid
  track: track, //req unless mbid
};
```

_\*Requires Authentication_

#### Remove Tag\*

[Last.FM Documentation](http://www.last.fm/api/show/track.removeTag)

`track_removeTag(opt)`, where

```ts
opt = {
  artist: artist, //req
  track: track, //req
  tag: tag, //req, single tag to remove
};
```

_\*Requires Authentication_

#### Scrobble\*

[Last.FM Documentation](http://www.last.fm/api/show/track.scrobble)

`track_scrobble(opt)`, where

```ts
opt = {
  artist: artist[i], //req
  track: track[i], //req
  timestamp: timestamp[i], //req
  album: album[i], //opt
  context: context[i], //opt
  streamId: streamId[i], //opt
  chosenByUser: chosenByUser[i], //opt
  trackNumber: trackNumber[i], //opt
  mbid: mbid[i], //opt
  albumArtist: albumArtist[i], //opt
  duration: duration[i], //opt
};
```

_\*Requires Authentication_

#### Search

[Last.FM Documentation](http://www.last.fm/api/show/track.search)

`track_search(opt)`, where

```ts
opt = {
  track: track, //req
  artist: artist, //opt
  limit: limit, //opt, defaults to 30
  page: page, //opt, defaults to 1
};
```

#### Unlove\*

[Last.FM Documentation](http://www.last.fm/api/show/track.unlove)

`track_unlove(opt)`, where

```ts
opt = {
  track: track, //req
  artist: artist, //req
};
```

_\*Requires Authentication_

#### Update Now Playing\*

[Last.FM Documentation](http://www.last.fm/api/show/track.updateNowPlaying)

`track_updateNowPlaying(opt)`, where

```ts
    opt = {
        artist: artist, //req
        track: track, //req
        album: album, //opt
        context: context //opt
        trackNumber: trackNumber, //opt
        mbid: mbid, //opt
        albumArtist: albumArtist, //opt
        duration: duration, //opt
    }
```

_\*Requires Authentication_

### User Methods

[Examples](../master/examples/user_examples.ts)

#### Get Artist Tracks

[Last.FM Documentation](http://www.last.fm/api/show/user.getArtistTracks)

`user_getArtistTracks(opt)`, where

```ts
opt = {
  user: username, //opt
  artist: artist, //req
  startTimestamp: startTimestamp, //opt defaults to all time
  page: page, //opt, default is 1
  endTimestamp: endTimestamp, //opt defaults to all time
};
```

#### Get Friends

[Last.FM Documentation](http://www.last.fm/api/show/user.getFriends)

`user_getFriends(opt)`, where

```ts
opt = {
  user: username, //opt
  recentTracks: recentTracks, //opt, true|false
  limit: limit, //opt defaults to 50
  page: page, //opt, default is 1
};
```

#### Get Info

[Last.FM Documentation](http://www.last.fm/api/show/user.getInfo)

`user_getInfo(opt)`, where

```ts
opt = {
  user: username, //opt, defaults to init user
};
```

#### Get Loved Tracks

[Last.FM Documentation](http://www.last.fm/api/show/user.getLovedTracks)

`user_getLovedTracks(opt)`, where

```ts
opt = {
  user: username, //opt
  limit: limit, //opt, default is 50
  page: page, //opt, default is 1
};
```

#### Get Personal Tags

[Last.FM Documentation](http://www.last.fm/api/show/user.getPersonalTags)

`user_getPersonalTags(opt)`, where

```ts
opt = {
  user: username, //opt
  tag: tag, //req
  taggingtype: artist | album | track, //req
  limit: limit, //opt, default is 50
  page: page, //opt, default is 1
};
```

#### Get Recent Tracks

[Last.FM Documentation](http://www.last.fm/api/show/user.getRecentTracks)

`user_getRecentTracks(opt)`, where

```ts
opt = {
  user: username, //opt
  from: startTime, //opt
  extended: 0 | 1, //opt
  to: endTime, //opt
  limit: limit, //opt, default is 50
  page: page, //opt, default is 1
};
```

#### Get Top Albums

[Last.FM Documentation](http://www.last.fm/api/show/user.getTopAlbums)

`user_getTopAlbums(opt)`, where

```ts
    opt = {
        user: username, //opt
        period: overall|7day|1month|3month|6month|12month, //opt, default is overall
        limit: limit, //opt, default is 50
        page: page, //opt, default is 1
    }
```

#### Get Top Artists

[Last.FM Documentation](http://www.last.fm/api/show/user.getTopArtists)

`user_getTopArtists(opt)`, where

```ts
    opt = {
        user: username, //opt
        period: overall|7day|1month|3month|6month|12month, //opt, default is overall
        limit: limit, //opt, default is 50
        page: page, //opt, default is 1
    }
```

#### Get Top Tags

[Last.FM Documentation](http://www.last.fm/api/show/user.getTopTags)

`user_getTopTags(opt)`, where

```ts
opt = {
  user: username, //opt
  limit: limit, //opt, default is 50
};
```

#### Get Top Tracks

[Last.FM Documentation](http://www.last.fm/api/show/user.getTopTracks)

`user_getTopTracks(opt)`, where

```ts
    opt = {
        user: username, //opt
        period: overall|7day|1month|3month|6month|12month, //opt, default is overall
        limit: limit, //opt, default is 50
        page: page, //opt, default is 1
    }
```

#### Get Weekly Album Chart

[Last.FM Documentation](http://www.last.fm/api/show/user.getWeeklyAlbumChart)

`user_getWeeklyAlbumChart(opt)`, where

```ts
opt = {
  user: username, //opt
  from: startdate, //opt, default is overall
  to: enddate, //opt, default is 50
};
```

#### Get Weekly Artist Chart

[Last.FM Documentation](http://www.last.fm/api/show/user.getWeeklyArtistChart)

`user_getWeeklyArtistChart(opt)`, where

```ts
opt = {
  user: username, //opt
  from: startdate, //opt, default is overall
  to: enddate, //opt, default is 50
};
```

#### Get Weekly Chart List

[Last.FM Documentation](http://www.last.fm/api/show/user.getWeeklyChartList)

`user_getWeeklyChartList(opt)`, where

```ts
opt = {
  user: username, //opt
};
```

#### Get Weekly Track Chart

[Last.FM Documentation](http://www.last.fm/api/show/user.getWeeklyTrackChart)

`user_getWeeklyTrackChart(opt)`, where

```ts
opt = {
  user: username, //opt
  from: startdate, //opt, default is overall
  to: enddate, //opt, default is 50
};
```
