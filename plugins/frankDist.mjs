import { basename, dirname, join, resolve } from "node:path";

import fsExtra from "fs-extra";

const { copy, readFile, writeFile } = fsExtra;

const packagePath = dirname(".");
const distPath = join(packagePath, "./dist");

const writeJson = (targetPath, obj) =>
  writeFile(targetPath, JSON.stringify(obj, null, 2), "utf8");

async function createPackageFile() {
  const packageData = await readFile(
    resolve(packagePath, "./package.json"),
    "utf8",
  );
  const { scripts, devDependencies, ...packageOthers } =
    JSON.parse(packageData);

  const newPackageData = {
    ...packageOthers,
    private: false,
    main: "./cjs/index.cjs",
    module: "./esm/index.mjs",
    types: "./index.d.ts",
    type: "module",
    exports: {
      require: "./cjs/index.cjs",
      import: "./esm/index.mjs",
      types: "./index.d.ts",
    },
    files: ["cjs/", "dataModel/", "esm/", "index.d.ts", "LastFM.d.ts"],
  };

  const targetPath = resolve(distPath, "./package.json");

  await writeJson(targetPath, newPackageData);
}

async function includeFileInBuild(file) {
  const sourcePath = resolve(packagePath, file);
  const targetPath = resolve(distPath, basename(file));
  await copy(sourcePath, targetPath);
}

/**
 * Rollup plugin to copy over readme and package.json
 * into the dist folder after build is complete
 * this lets us publish only the dist folder, and makes sure
 * consumers don't need to include `dist`
 * in the path when importing
 */
const frankDist = () => {
  return {
    name: "frank-dist-folder",
    generateBundle: async () => {
      try {
        await createPackageFile();
        await includeFileInBuild("./README.md");
      } catch (err) {
        console.error(err);

        process.exit(1);
      }
    },
  };
};

export default frankDist;
