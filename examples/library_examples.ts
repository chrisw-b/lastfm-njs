import LastFm from "../lib";
import config from "./config";

const lfm = new LastFm({
  apiKey: config.key,
  apiSecret: config.secret,
  username: config.username,
});

const printRes = function (res) {
  console.log(res);
};
const printError = function (error) {
  console.error(`ERROR: ${JSON.stringify(error)}`);
};

lfm
  .library_getArtists({
    user: "chrisw_b",
  })
  .then(printRes, printError);
