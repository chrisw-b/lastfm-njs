import LastFm from "../lib";
import config from "./config";

const lfm = new LastFm({
  apiKey: config.key,
  apiSecret: config.secret,
  username: config.username,
  password: config.password,
});

const printRes = function (res: string) {
  console.log(res);
};
const printError = function (error: Error) {
  console.error(`ERROR: ${JSON.stringify(error)}`);
};

lfm
  .auth_getMobileSession()
  .then(() => {
    lfm
      .artist_addTags({
        artist: "Lucy Dacus",
        tags: "folk,dacus,music",
      })
      .then(printRes);
    lfm
      .artist_removeTag({
        artist: "Lucy Dacus",
        tag: "music",
      })
      .then(printRes, printError);
  })
  .catch(printError);

lfm
  .artist_getCorrection({
    artist: "Guns and Roses",
  })
  .then(printRes)
  .catch(printError);

lfm
  .artist_getInfo({
    artist: "Thao & The Get Down Stay Down",
    username: "chrisw_b",
  })
  .then(printRes)
  .catch(printError);

lfm
  .artist_getSimilar({
    artist: "Waxahatchee",
    limit: 5,
  })
  .then(printRes)
  .catch(printError);

lfm
  .artist_getTags({
    artist: "Lucy Dacus",
    user: "chrisw_b",
  })
  .then(printRes)
  .catch(printError);

lfm
  .artist_getTopAlbums({
    artist: "A Camp",
    limit: 2,
  })
  .then(printRes, printError);

lfm
  .artist_getTopTags({
    artist: "Broken Social Scene",
  })
  .then(printRes, printError);

lfm
  .artist_getTopTracks({
    artist: "Shamir",
    limit: 5,
  })
  .then(printRes, printError);

lfm
  .artist_search({
    artist: "Stars",
    limit: 3,
  })
  .then(printRes, printError);
