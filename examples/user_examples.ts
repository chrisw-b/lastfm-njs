import LastFm from "../lib";
import config from "./config";

const lfm = new LastFm({
  apiKey: config.key,
  apiSecret: config.secret,
  username: config.username,
});

const printRes = function (res) {
  console.log(res);
};
const printError = function (error) {
  console.error(`ERROR: ${JSON.stringify(error)}`);
};

lfm
  .user_getArtistTracks({
    // user: 'chrisw_b', //defaults to authenticated user,
    artist: "Sylvan Esso",
  })
  .then(printRes, printError);

lfm
  .user_getFriends({
    // user: 'chrisw_b', //defaults to authenticated user,
    recenttracks: true,
  })
  .then(printRes, printError);

lfm.user_getInfo().then(printRes).catch(printError);

lfm.user_getLovedTracks().then(printRes).catch(printError);

lfm
  .user_getPersonalTags({
    // user: 'chrisw_b', //defaults to authenticated user,
    tag: "folk",
    taggingtype: "artist",
  })
  .then(printRes, printError);

lfm
  .user_getRecentTracks({
    // user: 'chrisw_b', //defaults to authenticated user,
    limit: 5,
  })
  .then(printRes, printError);

lfm
  .user_getTopAlbums({
    period: "1year",
  })
  .then(printRes, printError);

lfm.user_getTopArtists().then(printRes).catch(printError);

lfm
  .user_getTopTags({
    limit: 2,
  })
  .then(printRes, printError);

lfm.user_getTopTracks().then(printRes, printError);

lfm.user_getWeeklyAlbumChart().then(printRes, printError);

lfm.user_getWeeklyArtistChart().then(printRes, printError);

lfm.user_getWeeklyChartList().then(printRes, printError);

lfm.user_getWeeklyTrackChart().then(printRes, printError);
