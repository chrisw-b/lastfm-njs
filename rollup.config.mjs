import { readFileSync } from "node:fs";
import path from "node:path";

import { defineConfig } from "rollup";
import typescript from "rollup-plugin-typescript2";

import alias from "@rollup/plugin-alias";
import commonjs from "@rollup/plugin-commonjs";
import { nodeResolve } from "@rollup/plugin-node-resolve";

import frankDistFolder from "./plugins/frankDist.mjs";

const packageJson = JSON.parse(readFileSync("./package.json"));

const dirname = path.dirname(".");

const rollupOptions = defineConfig({
  input: "lib/index.ts",
  external: [
    ...(packageJson.dependencies ? Object.keys(packageJson.dependencies) : []),
    ...(packageJson.devDependencies
      ? Object.keys(packageJson.devDependencies)
      : []),
    ...(packageJson.peerDependencies
      ? Object.keys(packageJson.peerDependencies)
      : []),
  ],
  output: [
    {
      file: packageJson.module,
      format: "esm",
      sourcemap: true,
      exports: "named",
    },
    {
      file: packageJson.main,
      format: "cjs",
      sourcemap: true,
      exports: "named",
    },
  ],
  plugins: [
    // understand alias imports
    alias({ entries: [{ find: "~/", replacement: `${dirname}/lib/` }] }),
    // convert commonjs modules to es6
    commonjs(),
    nodeResolve(),
    // generate type declarations for typescript files
    typescript({
      useTsconfigDeclarationDir: true,
      tsconfigDefaults: {
        compilerOptions: {
          plugins: [
            {
              transform: "typescript-transform-paths",
              afterDeclarations: true,
            },
          ],
        },
      },
    }),

    frankDistFolder(),
  ],
});

export default rollupOptions;
